+++
isLastestVersion="true"
version="openEuler 1.0 Base"
description=""
status="Maintained"
releaseDate="2020/02/11"
installationGuide="https://openeuler.org/en/docs/Installation/Installation.html"
EOLDate="-"
ISOUrl="https://121.36.97.194/openeuler1.0/base/iso/"
+++
The openEuler 1.0 Base version is released to meet service requirements of specific users in specified scenarios and will be in the end-of-life (EOL) status after the openEuler 1.0 Standard version is released.

The openEuler 1.0 Standard version is a standard release version that meets open scenario requirements. It is planned as a long term service (LTS) version and has a lifecycle of four years.