+++
isLastestVersion="true"
version="openEuler 1.0 Base"
description=""
status="Maintained"
releaseDate="2020/02/11"
releaseNote="https://openeuler.org/zh/docs/Releasenotes/release_notes.html"
installationGuide="https://openeuler.org/zh/docs/Installation/installation.html"
EOLDate="-"
ISOUrl="https://121.36.97.194/openeuler1.0/base/iso/"
+++
openEuler 1.0 Base版本是为了满足特定用户指定场景业务需求发布的版本，在openEuler 1.0 Standard版本发布后，本版本停止服务。

openEuler 1.0 Standard 版本是满足开放场景的标准发行版，规划作为LTS版本，生命周期四年。